A standard front end starter pack which includes:

- Webpack
- SASS
- Bootstrap
- Babel
- ES6
- JQuery